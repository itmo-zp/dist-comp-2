#include <stdlib.h>
#include <string.h>
#include "skipped_messages.h"

void skipMessage(SkippedMessages* skipped_messages, const SkippedMessage* message) {
    skipped_messages->messages = realloc(skipped_messages->messages, sizeof(SkippedMessage) * (++skipped_messages->messages_count));
    memcpy(&skipped_messages->messages[skipped_messages->messages_count - 1], message, sizeof(SkippedMessage));
}
