#include <stdio.h>
#include <unistd.h>
#include "log.h"
#include "pa2345.h"
#include "process.h"
#include "banking.h"

void log_pipe(const ProcessPipe* pipe_from, const ProcessPipe* pipe_to) {
    printf(
            log_pipe_fmt,
            pipe_to->id, pipe_from->id,
            pipe_from->in, pipe_to->in,
            pipe_from->out, pipe_to->out
    );
    fprintf(
            pipe_log_file,
            log_pipe_fmt,
            pipe_to->id, pipe_from->id,
            pipe_from->in, pipe_to->in,
            pipe_from->out, pipe_to->out
    );
    fflush(pipe_log_file);
}

void log_started(const Process* process) {
    printf(
            log_started_fmt,
            get_physical_time(),
            process->id,
            getpid(),
            getppid(),
            process->balance_history.s_history[0].s_balance
    );
    fprintf(
            event_log_file,
            log_started_fmt,
            get_physical_time(),
            process->id,
            getpid(),
            getppid(),
            process->balance_history.s_history[0].s_balance
    );
    fflush(event_log_file);
}

void log_received_all_started(const Process* process) {
    printf(log_received_all_started_fmt, get_physical_time(), process->id);
    fprintf(event_log_file, log_received_all_started_fmt, get_physical_time(), process->id);
    fflush(event_log_file);
}

void log_done(const Process* process) {
    printf(
            log_done_fmt,
            get_physical_time(),
            process->id,
            process->balance_history.s_history[process->balance_history.s_history_len - 1].s_balance
    );
    fprintf(
            event_log_file,
            log_done_fmt,
            get_physical_time(),
            process->id,
            process->balance_history.s_history[process->balance_history.s_history_len - 1].s_balance
    );
    fflush(event_log_file);
}

void log_received_all_done(const Process* process) {
    printf(log_received_all_done_fmt, get_physical_time(), process->id);
    fprintf(event_log_file, log_received_all_done_fmt, get_physical_time(), process->id);
    fflush(event_log_file);
}

void log_transfer_out(local_id src, local_id dst, balance_t amount) {
    printf(log_transfer_out_fmt, get_physical_time(), src, amount, dst);
    fprintf(event_log_file, log_transfer_out_fmt, get_physical_time(), src, amount, dst);
    fflush(event_log_file);
}

void log_transfer_in(local_id src, local_id dst, balance_t amount) {
    printf(log_transfer_in_fmt, get_physical_time(), dst, amount, src);
    fprintf(event_log_file, log_transfer_out_fmt, get_physical_time(), dst, amount, src);
    fflush(event_log_file);
}
