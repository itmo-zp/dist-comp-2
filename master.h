#ifndef __IFMO_DISTRIBUTED_CLASS_MASTER__H
#define __IFMO_DISTRIBUTED_CLASS_MASTER__H

#include "process.h"

int startMaster(const Process *master);

#endif // __IFMO_DISTRIBUTED_CLASS_MASTER__H
