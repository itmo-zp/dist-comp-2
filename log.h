#ifndef __IFMO_DISTRIBUTED_CLASS_LOG__H
#define __IFMO_DISTRIBUTED_CLASS_LOG__H

#include "process.h"

extern FILE* event_log_file;
extern FILE* pipe_log_file;

static const char* const log_pipe_fmt = "Pipes between %d and %d: read(%d, %d), write(%d, %d)\n";

void log_pipe(const ProcessPipe* pipe_to, const ProcessPipe* pipe_from);

void log_started(const Process* process);

void log_received_all_started(const Process* process);

void log_done(const Process* process);

void log_received_all_done(const Process* process);

void log_transfer_out(local_id src, local_id dst, balance_t amount);

void log_transfer_in(local_id src, local_id dst, balance_t amount);

#endif // __IFMO_DISTRIBUTED_CLASS_LOG__H
