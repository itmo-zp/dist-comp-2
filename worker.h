#ifndef __IFMO_DISTRIBUTED_CLASS_WORKER__H
#define __IFMO_DISTRIBUTED_CLASS_WORKER__H

#include "process.h"

int startWorker(Process *process);

#endif // __IFMO_DISTRIBUTED_CLASS_WORKER__H
