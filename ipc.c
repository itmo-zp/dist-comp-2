#include <stdio.h>
#include <fcntl.h>
#include "ipc.h"
#include "unistd.h"
#include "process.h"

static int send_msg(int pipe_out, const Message* msg) {
    size_t nbyte = sizeof(MessageHeader) + msg->s_header.s_payload_len;
    return nbyte != write(
            pipe_out,
            msg,
            nbyte
    );
}

static int receive_msg(int pipe_in, Message* msg) {
    size_t nbyte = sizeof(MessageHeader);
    return nbyte != read(
            pipe_in,
            &msg->s_header,
            nbyte
    ) || msg->s_header.s_payload_len != read(
            pipe_in,
            msg->s_payload,
            msg->s_header.s_payload_len
    );
}

int send(void* self, local_id dst, const Message* msg) {
    Process* worker = self;

    for (local_id i = 0; i < worker->pipes_count; i++) {
        if (worker->pipes[i].id == dst) {
            return send_msg(worker->pipes[i].out, msg);
        }
    }

    return -1;
}

int send_multicast(void* self, const Message* msg) {
    Process* worker = self;

    for (local_id i = 0; i < worker->pipes_count; i++) {
        if (send_msg(worker->pipes[i].out, msg)) {
            return 1;
        }
    }

    return 0;
}

int receive(void* self, local_id from, Message* msg) {
    Process* worker = self;

    for (local_id i = 0; i < worker->pipes_count; i++) {
        if (worker->pipes[i].id == from) {
            return receive_msg(worker->pipes[i].in, msg);
        }
    }

    return -1;
}

int receive_any(void *self, Message *msg) {
    Process *worker = self;
    size_t nbyte = sizeof(MessageHeader);

    for (local_id i = 0; i < worker->pipes_count; i++) {
        if (nbyte == read(
                worker->pipes[i].in,
                &msg->s_header,
                nbyte
        )) {
            return msg->s_header.s_payload_len != read(
                    worker->pipes[i].in,
                    msg->s_payload,
                    msg->s_header.s_payload_len
            );
        }
    }

    return -1;
}
