#ifndef __IFMO_DISTRIBUTED_CLASS_SKIP_MESSAGES__H
#define __IFMO_DISTRIBUTED_CLASS_SKIP_MESSAGES__H

#include "ipc.h"

typedef struct {
    local_id from;
    Message message;
} SkippedMessage;

typedef struct {
    int messages_count;
    SkippedMessage* messages;
} SkippedMessages;

void skipMessage(SkippedMessages* skipped_messages, const SkippedMessage* message);

#endif // __IFMO_DISTRIBUTED_CLASS_SKIP_MESSAGES__H
