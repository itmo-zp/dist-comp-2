#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <fcntl.h>
#include "process.h"
#include "log.h"
#include "common.h"
#include "master.h"
#include "worker.h"

FILE* event_log_file;
FILE* pipe_log_file;

static int initWorkers(local_id proc_count, const balance_t* balances);

int main(int argc, char** argv) {
    local_id proc_count = 0;
    balance_t* balances = NULL;

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-p") == 0) {
            if (++i >= argc) {
                printf("Usage: %s -p N S..\n", argv[0]);
                return 1;
            }
            proc_count = (local_id) strtol(argv[i], NULL, 10);
            if (errno != 0 || proc_count < 2 || proc_count > 10) {
                printf("Illegal process count: should be [2; 10]\n");
                return 1;
            }
            if (argc <= i + proc_count) {
                printf("Should have %d balance values", proc_count);
                return 1;
            }
            balances = realloc(balances, sizeof(short) * proc_count);
            for (int j = 0; j < proc_count; j++) {
                balance_t balance = (balance_t) strtol(argv[++i], NULL, 10);
                if (errno != 0 || balance < 1 || balance > 99) {
                    printf("Illegal balance: should be [1; 99]\n");
                    return 1;
                }
                balances[j] = balance;
            }
        } else {
            printf("Illegal option %s\n", argv[i]);
            return 1;
        }
    }

    if (proc_count == 0) {
        printf("Usage: %s -p N S..\n", argv[0]);
        return 1;
    }

    event_log_file = fopen(events_log, "w");
    if (event_log_file == NULL) {
        perror("fopen");
        return 1;
    }
    pipe_log_file = fopen(pipes_log, "w");
    if (pipe_log_file == NULL) {
        perror("fopen");
        return 1;
    }

    return initWorkers(proc_count, balances);
}

int initWorkers(const local_id proc_count, const balance_t* balances) {
    Process workers[proc_count];
    int pipe_to[2], pipe_from[2];

    Process master_process = {
            .id = PARENT_ID,
            .pipes = malloc(proc_count * sizeof(ProcessPipe)),
            .pipes_count = proc_count
    };

    for (local_id worker_index = 0; worker_index < proc_count; worker_index++) {
        if (pipe(pipe_to) == -1 || pipe(pipe_from) == -1) {
            perror("pipe");
            return 1;
        }
        if (
                fcntl(pipe_to[0], F_SETFL, fcntl(pipe_to[0], F_GETFL) | O_NONBLOCK)
                || fcntl(pipe_from[0], F_SETFL, fcntl(pipe_from[0], F_GETFL) | O_NONBLOCK)
                || fcntl(pipe_to[1], F_SETFL, fcntl(pipe_to[1], F_GETFL) | O_NONBLOCK)
                || fcntl(pipe_from[1], F_SETFL, fcntl(pipe_from[1], F_GETFL) | O_NONBLOCK)

        ) {
            perror("fcntl");
            return 1;
        }
        ProcessPipe worker_pipe = {
                .id = worker_index + (local_id)1,
                .in = pipe_to[0],
                .out = pipe_from[1]
        };
        ProcessPipe master_pipe = {
                .id = PARENT_ID,
                .in = pipe_from[0],
                .out = pipe_to[1]
        };

        master_process.pipes[worker_index] = worker_pipe;
        workers[worker_index] = (Process) {
                .id = worker_index + (local_id)1,
                .balance_history = (BalanceHistory) {
                        .s_id = worker_index + (local_id)1,
                        .s_history_len = 1,
                        .s_history[0] = (BalanceState) {
                                .s_balance = balances[worker_index],
                                .s_time = get_physical_time(),
                                .s_balance_pending_in = 0
                        }
                },
                .pipes_count = proc_count,
                .pipes = malloc((proc_count) * sizeof(ProcessPipe))
        };
        workers[worker_index].pipes[0] = master_pipe;
        log_pipe(&worker_pipe, &master_pipe);
    }
    for (local_id root_worker_index = 0; root_worker_index < proc_count; root_worker_index++) {
        Process root_worker = workers[root_worker_index];
        for (local_id worker_index = root_worker_index + (local_id)1; worker_index < proc_count; worker_index++) {
            if (pipe(pipe_to) == -1 || pipe(pipe_from) == -1) {
                perror("pipe");
                return 1;
            }
            if (
                    fcntl(pipe_to[0], F_SETFL, fcntl(pipe_to[0], F_GETFL) | O_NONBLOCK)
                    || fcntl(pipe_from[0], F_SETFL, fcntl(pipe_from[0], F_GETFL) | O_NONBLOCK)
                    || fcntl(pipe_to[1], F_SETFL, fcntl(pipe_to[1], F_GETFL) | O_NONBLOCK)
                    || fcntl(pipe_from[1], F_SETFL, fcntl(pipe_from[1], F_GETFL) | O_NONBLOCK)
            ) {
                perror("fcntl");
                return 1;
            }
            ProcessPipe proc_pipe_to = {
                    .id = worker_index + (local_id)1,
                    .in = pipe_from[0],
                    .out = pipe_to[1]
            };
            ProcessPipe proc_pipe_from = {
                    .id = root_worker_index + (local_id)1,
                    .in = pipe_to[0],
                    .out = pipe_from[1]
            };
            root_worker.pipes[worker_index] = proc_pipe_to;
            workers[worker_index].pipes[1 + root_worker_index] = proc_pipe_from;
            log_pipe(&proc_pipe_to, &proc_pipe_from);
        }
    }

    pid_t worker_pids[proc_count];
    for (int i = 0; i < proc_count; i++) {
        pid_t worker_pid;
        if ((worker_pid = fork()) == -1) {
            perror("fork");
            return 1;
        }

        if (worker_pid == 0) {
            for (local_id worker_index = 0; worker_index < proc_count; worker_index++) {
                close(master_process.pipes[worker_index].in);
                close(master_process.pipes[worker_index].out);
                if (i == worker_index) {
                    continue;
                }
                Process worker = workers[worker_index];
                for (local_id pipe_index = 0; pipe_index < worker.pipes_count; pipe_index++) {
                    close(worker.pipes[pipe_index].in);
                    close(worker.pipes[pipe_index].out);
                }
            }

            return startWorker(&workers[i]);
        } else {
            worker_pids[i] = worker_pid;
            Process worker = workers[i];
            for (local_id pipe_index = 0; pipe_index < worker.pipes_count; pipe_index++) {
                close(worker.pipes[pipe_index].in);
                close(worker.pipes[pipe_index].out);
            }
        }
    }

    startMaster(&master_process);

    for (int i = 0; i < proc_count; i++) {
        waitpid(worker_pids[i], NULL, 0);
    }

    return 0;
}
